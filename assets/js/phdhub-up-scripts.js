/* jQuery(document).ready(function($) {
  $("#repeat-password").keyup(validate);
});

function validate() {
	(function($){
		var password1 = $("#password").val();
		var password2 = $("#repeat-password").val();

		if( (password1.length == 0) && (password2.length == 0) ) {
			$("#password").css("border-color","#e8e8e8");
			$("#repeat-password").css("border-color","#e8e8e8");
			$("#message").text("").css({"display":"none"});  
		} else {
			if(password1 == password2) {
				$("#password").css("border-color","green");
				$("#repeat-password").css("border-color","green");
				$("#message").text("Passwords match").css({"display":"block","font-size":"12px","margin":"5px 0 0"});  
			}
			else {
				$("#password").css("border-color","red");
				$("#repeat-password").css("border-color","red");
				$("#message").text("Passwords don't match").css({"display":"block","font-size":"12px","margin":"5px 0 0"});  
			}
		}
	}(jQuery));
} */

function checkPasswordStrength( $pass1,
                                $pass2,
                                $strengthResult,
                                $submitButton,
                                blacklistArray ) {
        var pass1 = $pass1.val();
    var pass2 = $pass2.val();
 
    // Reset the form & meter
    $submitButton.attr( 'disabled', 'disabled' );
    $strengthResult.removeClass( 'short bad good strong' );
 
    // Extend our blacklist array with those from the inputs & site data
    blacklistArray = blacklistArray.concat( wp.passwordStrength.userInputBlacklist() )
 
    // Get the password strength
    var strength = wp.passwordStrength.meter( pass1, blacklistArray, pass2 );
 
    // Add the strength meter results
    switch ( strength ) {
 
        case 2:
            $strengthResult.addClass( 'bad' ).html( pwsL10n.bad ).css({"color":"red","display":"block","font-size":"12px","margin":"5px 0 0"});
            break;
 
        case 3:
            $strengthResult.addClass( 'good' ).html( pwsL10n.good ).css({"color":"orange","display":"block","font-size":"12px","margin":"5px 0 0"});
            break;
 
        case 4:
            $strengthResult.addClass( 'strong' ).html( pwsL10n.strong ).css({"color":"green","display":"block","font-size":"12px","margin":"5px 0 0"});
            break;
 
        case 5:
            $strengthResult.addClass( 'short' ).html( pwsL10n.mismatch ).css({"color":"red","display":"block","font-size":"12px","margin":"5px 0 0"});
            break;
 
        default:
            $strengthResult.addClass( 'short' ).html( pwsL10n.short ).css({"color":"red","display":"block","font-size":"12px","margin":"5px 0 0"});
 
    }
 
    // The meter function returns a result even if pass2 is empty,
    // enable only the submit button if the password is strong and
    // both passwords are filled up
    if ( ( (4 === strength) || (3 === strength) ) && '' !== pass2.trim() ) {
        $submitButton.removeAttr( 'disabled' );
    }
 
    return strength;
}
 
jQuery( document ).ready( function( $ ) {
    // Binding to trigger checkPasswordStrength
    $( 'body' ).on( 'keyup', 'input[name=password], input[name=repeat_password]',
        function( event ) {
            checkPasswordStrength(
                $('input[name=password]'),         // First password field
                $('input[name=repeat_password]'), // Second password field
                $('#password-strength'),           // Strength meter
                $('input[type=submit]'),           // Submit button
                ['black', 'listed', 'word']        // Blacklisted words
            );
        }
    );
});
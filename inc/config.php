<?php
/*
 * PhD Hub User Pages - Files
 * Include all required files
 */
defined('ABSPATH') or die;

/*
 * Core Files
 */
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phdhub-up-functions.php'; // Custom Functions file
include_once dirname(plugin_dir_path(__FILE__)) . '/inc/config/phdhub-up-scripts.php'; // Load CSS/JS Scripts

?>
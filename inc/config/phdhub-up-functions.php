<?php

defined('ABSPATH') or die;

/*
 * Register Page Template
 */
function phdhub_up_page_templates($templates) {
    $templates['login-page.php'] = 'User Login Page';
    $templates['profile-page.php'] = 'User Profile Page';
    $templates['settings-page.php'] = 'User Account Settings Page';
    $templates['password-settings-page.php'] = 'Password Settings Page';
    return $templates;
}
add_filter ('theme_page_templates', 'phdhub_up_page_templates');

/*
 * Load Page Template
 */
function phdhub_up_redirect_page_templates($template) {
	if(  get_page_template_slug() === 'login-page.php' ) {
        if ( $theme_file = locate_template( array( 'login-page.php' ) ) ) {
            $template = $theme_file;
        } else {
            $template = dirname(plugin_dir_path(__DIR__)) . '/pages/login-page.php';
        }
    }
	
	if(  get_page_template_slug() === 'profile-page.php' ) {
        if ( $theme_file = locate_template( array( 'profile-page.php' ) ) ) {
            $template = $theme_file;
        } else {
            $template = dirname(plugin_dir_path(__DIR__)) . '/pages/profile-page.php';
        }
	}
	
	if(  get_page_template_slug() === 'settings-page.php' ) {
        if ( $theme_file = locate_template( array( 'settings-page.php' ) ) ) {
            $template = $theme_file;
        } else {
            $template = dirname(plugin_dir_path(__DIR__)) . '/pages/settings-page.php';
        }
	}
	
	if(  get_page_template_slug() === 'password-settings-page.php' ) {
        if ( $theme_file = locate_template( array( 'password-settings-page.php' ) ) ) {
            $template = $theme_file;
        } else {
            $template = dirname(plugin_dir_path(__DIR__)) . '/pages/password-settings-page.php';
        }
	}

	/*
    if($template == '') {
        throw new \Exception('No template found');
    } */
    return $template;
}
add_filter ('template_include', 'phdhub_up_redirect_page_templates');

/* REDIRECT SUBSCRIBERS TO PROFILE */
function subscriber_login_redirect( $redirect_to, $request, $user ) {
    //is there a user to check?
    if (isset($user->roles) && is_array($user->roles)) {
        //check for subscribers
        // if (in_array('subscriber', $user->roles)) {
	$sites = get_sites();
    	foreach ( $sites as $site ) {
        	switch_to_blog( $site->blog_id );
		if ( is_user_member_of_blog( $user->ID, $site->blog_id )) {
			$redirect_to =  site_url() . '/profile';
		}
		else {
			//if ( ! current_user_can( 'manage_options' ) || ! current_user_can('manage_categories') ) {
            			// redirect them to another URL, in this case, the homepage 
            			$redirect_to =  'https://phdhub.eu/profile';
			//}
        	}
		restore_current_blog();
	}
    }
    return $redirect_to;
}

add_filter( 'login_redirect', 'subscriber_login_redirect', 100, 3 );

/* HIDE ADMIN BAR FOR SUBSCRIBERS */
//add_action('after_setup_theme', 'disable_admin_bar');
function disable_admin_bar() {
   // if ( ! current_user_can( 'manage_options' ) || ! current_user_can('manage_categories') ) {
	//global $current_user;
	//$user_roles = $current_user->roles;
	//$user_role = array_shift($user_roles);
	//if ($user_role === 'subscriber') {
    global $current_user; // Use global
    wp_get_current_user(); // Make sure global is set, if not set it.
    if ( user_can( $current_user, "subscriber" ) ) {
		show_admin_bar( false );
	}
}

/* DISABLE ACCESS TO WP-ADMIN FOR SUBSCRIBERS */
function subscriber_disable_admin_access() {
    $redirect = site_url() . '/profile';
    // global $current_user;
    // $current_user = wp_get_current_user();
    global $current_user; // Use global
    wp_get_current_user(); // Make sure global is set, if not set it.
    if ( user_can( $current_user, "subscriber" ) ) {
    // $user_roles = $current_user->roles;
    // $user_role = array_shift($user_roles);
    // if($user_role === 'subscriber'){
    // if ( ! current_user_can( 'manage_options' ) ) {
        exit( wp_redirect( $redirect ) );
    }
 }
//add_action( 'admin_init', 'subscriber_disable_admin_access', 100 );

add_action( 'signup_extra_fields', 'phdhub_registration_extra_fields', 10, 2 );
function phdhub_registration_extra_fields() {
?>
<p><input type="checkbox" name="accept_pp" id="accept_pp" required="required"> <?php echo __('I agree to the', 'phdhub-up') . ' ' . '<a href="/privacy-policy">' . __('Privacy Policy', 'phdhub-up') . '</a> and ' . '<a href="/terms-and-conditions">' . __('Terms and Conditions', 'phdhub-up') . '</a>.'; ?></p>
<?php
}
?>

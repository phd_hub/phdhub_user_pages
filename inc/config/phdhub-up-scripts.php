<?php

defined('ABSPATH') or die;

/* 
 * Enqueue CSS & JS Files for PhD Hub User Pages Plugin - Frontend
 */
function phdhub_up_scripts() {
		wp_register_style('phdhub-up-style', plugins_url( PHDHUB_USER_PAGES_PLUGIN_DIR . '/assets/css/style.css'));
		wp_enqueue_style('phdhub-up-style');
		wp_register_style('phdhub-up-uikit-sticky', plugins_url( PHDHUB_USER_PAGES_PLUGIN_DIR . '/assets/css/sticky.css'));
		wp_enqueue_style('phdhub-up-uikit-sticky');
	
		wp_enqueue_script( 'password-strength-meter' );
		wp_register_script('phdhub-up-scripts', plugins_url( PHDHUB_USER_PAGES_PLUGIN_DIR . '/assets/js/phdhub-up-scripts.js'), array('jquery'));
		wp_enqueue_script('phdhub-up-scripts');
		wp_register_script('phdhub-up-uikit-sticky', plugins_url( PHDHUB_USER_PAGES_PLUGIN_DIR . '/assets/js/sticky.min.js'), array('jquery'));
		wp_enqueue_script('phdhub-up-uikit-sticky');
}
add_action( 'wp_enqueue_scripts', 'phdhub_up_scripts', 100 );

?>
<?php
defined('ABSPATH') or die;

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div id="login-register-page">
				<header class="entry-header cpt-header">
					<div class="container">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</div>
				</header><!-- .entry-header -->
				<div class="container">
					<?php
						if (is_user_logged_in()) {
							$current_user = wp_get_current_user();
					?>
					<div class="is-logged-in">
						<div class="uk-grid">
							<div class="uk-width-7-10">
								<h3><?php echo __('Welcome', 'phdhub-up') . ' ' . $current_user->user_firstname; ?></h3>
								<p>
									<?php echo __('It seems that you have already logged in to the PhD Hub platform.', 'phdhub-up'); ?>
								</p>
								<p class="notice">
									<?php echo __('If this is not your account, please log out immediately and then login with your own account!', 'phdhub-up'); ?>
								</p>
								<p class="logout">
									<a href="<?php echo wp_logout_url( home_url() ); ?>"><?php echo __('Logout', 'phdhub-up'); ?></a>
								</p>
							</div>
							
							<div class="uk-width-3-10">
								<h3><?php echo __('My Account', 'phdhub-up'); ?></h3>
								<ul class="account-links">
									<li><i class="fa fa-user-circle-o"></i> <a href="<?php echo site_url() . '/profile'; ?>"><?php echo __('Profile', 'phdhub-up'); ?></a></li>
									<li><i class="fa fa-cogs"></i> <a href="<?php echo site_url() . '/account-settings'; ?>"><?php echo __('Settings', 'phdhub-up'); ?></a></li>
								</ul>
							</div>
						</div>
					</div>
					<?php
						}
						else {
					?>
					<div class="is-logged-out uk-grid">
						<div class="uk-width-1-2">
							<div class="register-box">
								<p>
									<?php echo __('Do you have an account?', 'phdhub-up'); ?>
								</p>
								<p>
									<span><?php echo __('Use the following link in order to get access to the PhD Hub platform.', 'phdhub-up'); ?></span>
									<a href="<?php echo wp_login_url( home_url() ); ?>"><?php echo __('Login', 'phdhub-up'); ?></a>
								</p>
							</div>
						</div>
						<div class="uk-width-1-2">
							<div class="register-box">
								<p>
									<?php echo __('Are you a new user?', 'phdhub-up'); ?>
								</p>
								<p>
									<span><?php echo __('Use the following link in order to create a new account and get access to the PhD Hub platform.', 'phdhub-up'); ?></span>
									<a href="<?php echo site_url() . '/wp-signup.php'; ?>"><?php echo __('Create an account', 'phdhub-up'); ?></a>
								</p>
							</div>
						</div>
					</div>
					<?php
						}
					?>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

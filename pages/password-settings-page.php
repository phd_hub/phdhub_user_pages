<?php
	defined('ABSPATH') or die;

	get_header();

	$password = (isset($_POST['password']) ? $_POST['password'] : null);
	$update_account = (isset($_POST['update_account']) ? $_POST['update_account'] : null);

	$current_user = wp_get_current_user();
	$user_id = $current_user->ID;
	if ($update_account) {
		if (!empty ($password)) {
			$userdata = array(
				'ID' => $user_id,
				'user_pass'   =>  $password
			);
		}
	}
	$user_info = get_userdata($user_id);
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<div id="registration-page">
			<header class="entry-header cpt-header">
				<div class="container">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div>
			</header><!-- .entry-header -->
			
			<div class="container">
				<div class="uk-grid">
					<div class="uk-width-7-10">
						<?php
							if ( is_user_logged_in()) {
						?>
						<form class="registration-form" method="POST">
							<div class="uk-grid">
								<div class="uk-width-1-1">
									<h3><?php echo __('Reset your password', 'phdhub-eu'); ?></h3>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('Password', 'phdhub-up'); ?></label>
										<input type="password" id="password" name="password">
										<span id="password-strength"></span>
									</p>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('Repeat Password', 'phdhub-up'); ?></label>
										<input type="password" id="repeat-password" name="repeat_password">
									</p>
								</div>
							</div>
							<input type="submit" name="update_account" value="<?php echo __('Update Account', 'phdhub-up'); ?>" />
						</form>
						<?php
							} else {
								wp_redirect( site_url() . '/login' );
							}
						?>
					</div>
					
					<div class="uk-width-3-10">
						<?php
							if (! is_user_logged_in() ) {
						?>
						<div data-uk-sticky="{boundary: true}">
							<div class="login-box-link">
								<p>
									<span><?php echo __('If you already have an account, please login using the following link', 'phdhub-up'); ?>:</span>
									<a href="<?php echo site_url() . '/login'; ?>"><?php echo __('Login Now', 'phdhub-up'); ?></a>
								</p>
							</div>
						</div>
						<?php
							} else {
						?>
						<div class="is-logged-in">
							<h3><?php echo __('My Account', 'phdhub-up'); ?></h3>
							<ul class="account-links">
								<li><i class="fa fa-user-circle-o"></i> <a href="<?php echo site_url() . '/profile'; ?>"><?php echo __('Profile', 'phdhub-up'); ?></a></li>
								<li><i class="fa fa-cogs"></i> <a href="<?php echo site_url() . '/account-settings'; ?>"><?php echo __('Account Settings', 'phdhub-up'); ?></a></li>
								<li><i class="fa fa-lock"></i> <a href="<?php echo site_url() . '/password-settings'; ?>"><?php echo __('Password Settings', 'phdhub-up'); ?></a></li>
							</ul>
						</div>
						<?php
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</main>
</div>
<?php
	get_footer();
?>
<?php
defined('ABSPATH') or die;

get_header();
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
			<div id="profile-page">
				<header class="entry-header cpt-header">
					<div class="container">
						<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
					</div>
				</header><!-- .entry-header -->
				<div class="container">
					<?php
						if (is_user_logged_in()) {
							$current_user = wp_get_current_user();
							$user_id = $current_user->ID;
							$user_info = get_userdata($user_id);
					?>
					<div class="is-logged-in">
						<div class="uk-grid">
							<div class="uk-width-3-10">
								<div class="user-details-block">
									<?php echo get_avatar( $current_user->ID ); ?>
									<div class="user-info-edit">
										<p><?php echo $current_user->user_firstname . ' ' . $current_user->user_lastname; ?></p>
										<p><a href="<?php echo site_url() . '/account-settings'; ?>"><?php echo __('Edit Account', 'phdhub-up'); ?></a></p>
										<?php
											if ( in_array( 'subscriber', (array) $current_user->roles ) ) {
										?>
										<p><?php echo do_shortcode( '[plugin_delete_me /]' ); ?></p>
										<?php
											}
										?>
										<a href="#personal-data" data-uk-modal><?php echo __('Export Personal Data', 'phdhub-up'); ?></a>
									</div>
									<div class="uk-grid">
										<div class="uk-width-1-2">
											<p>
												<label><?php echo __('First Name', 'phdhub-up'); ?>:</label>
												<span class="user-profile-value"><?php echo $current_user->user_firstname; ?></span>
											</p>
										</div>
										<div class="uk-width-1-2">
											<p>
												<label><?php echo __('Last Name', 'phdhub-up'); ?>:</label>
												<span class="user-profile-value"><?php echo $current_user->user_lastname; ?></span>
											</p>
										</div>
										<div class="uk-width-1-1">
											<p>
												<label><?php echo __('Email', 'phdhub-up'); ?>:</label>
												<span class="user-profile-value"><?php echo $current_user->user_email; ?></span>
											</p>
										</div>
										<div class="uk-width-1-1">
											<p>
												<label><?php echo __('Website', 'phdhub-up'); ?>:</label>
												<span class="user-profile-value"><?php echo $current_user->user_url; ?></span>
											</p>
										</div>
										<div class="uk-width-1-1">
											<p>
												<label><?php echo __('Current User Roles', 'phdhub-up') . ': '; ?></label>
												<span class="user-profile-value">
													<?php 
														if ( is_user_member_of_blog( $current_user->ID ) ) {
															global $wp_roles;
															$user_roles = $current_user->roles;		
															$user_role = array_shift($user_roles);
															echo $wp_roles->roles[ $user_role ]['name'];
														}
														else {
															echo __('Registered Member','phdhub-up');
														}
													?>
												</span>
											</p>
										</div>
										<?php 
											$user_fb = esc_url( get_user_meta($current_user->ID, 'fb_link', true) );
											$user_twitter = esc_url( get_user_meta($current_user->ID, 'twitter_link', true) );
											$user_linkedin = esc_url( get_user_meta($current_user->ID, 'linkedin_link', true) );
											$user_gplus = esc_url( get_user_meta($current_user->ID, 'gplus_link', true) );
							
											if ( (!empty ($user_fb)) || (!empty ($user_twitter)) || (!empty ($user_linkedin)) || (!empty ($user_gplus)) ) {
										?>
										<div class="uk-width-1-1 profile-area-sep">
											<p>
												<label><?php echo __('Social Networks', 'phdhub-up'); ?></label>
												<?php
													if (!empty ($user_fb)) {
												?>
												<a href="<?php echo $user_fb; ?>"><i class="fa fa-facebook"></i></a>
												<?php
													}
													if (!empty ($user_twitter)) {
												?>
												<a href="<?php echo $user_twitter; ?>"><i class="fa fa-twitter"></i></a>
												<?php
													}
													if (!empty ($user_linkedin)) {
												?>
												<a href="<?php echo $user_linkedin; ?>"><i class="fa fa-linkedin"></i></a>
												<?php
													}
													if (!empty ($user_gplus)) {
												?>
												<a href="<?php echo $user_gplus; ?>"><i class="fa fa-google-plus"></i></a>
												<?php
													}
												?>
										</div>
										<?php
											}
										?>
									</div>
								</div>
							</div>
							
							<div class="uk-width-6-10">
								<?php
									$qualifications = wp_get_object_terms( $current_user->ID, 'qualifications' );
									if (!empty ($qualifications)) {
								?>
								<div class="user-qualifications-block">
									<h3><?php echo __('Qualifications', 'phdhub-up'); ?></h3>
									<p>
										<?php 
											foreach( $qualifications as $term ) {
												$user_qualifications[] = '<a href="' . get_term_link($term->term_id) . '">' . $term->name . '</a>';
											}
											$user_tags = implode( ', ', $user_qualifications );
											echo $user_tags;
										?>
									</p>
								</div>
								<?php
									}
									if ( in_array( 'subscriber', (array) $current_user->roles ) ||  in_array( 'lead-researcher', (array) $current_user->roles ) || in_array( 'assist-researcher', (array) $current_user->roles ) ) {
										$fields_of_science = get_the_author_meta( 'fields_of_science', $current_user->ID );
										if (!empty ($fields_of_science)) {
								?>
								<div class="user-fields-of-interest-block">
									<h3><?php echo __('Interested in the following Fields of Science', 'phdhub-up'); ?></h3>
									<?php
										if (!empty ($fields_of_science)) {
											foreach ($fields_of_science as $fos) {
												$field_of_science = get_term( $fos, 'fields-of-science' );
												$user_fos[]  = $field_of_science->name;
											}
											$user_fields_of_science = implode( ', ', $user_fos );
											echo $user_fields_of_science;
										}
									?>
								</div>
								<?php
										}
									}
								?>
								<div class="uk-grid favorites-search-history">
									<div class="uk-width-1-1">
										<div class="favorites-block">
											<h3>
												<?php echo __('Favorites', 'phdhub-up') . ': ' . get_user_favorites_count($current_user->ID); ?>
												<?php the_clear_favorites_button(); ?>
											</h3>
											<?php
												the_user_favorites_list( $current_user->ID );
											?>
										</div>
									</div>
									
									<div class="uk-width-1-1">
										
									</div>
								</div>
							</div>
							
							<div class="uk-width-1-10">
								<p class="logout">
									<a href="<?php echo wp_logout_url( home_url() ); ?>"><?php echo __('Logout', 'phdhub-up'); ?></a>
								</p>
							</div>
						</div>
						
					</div>
					<?php
						}
						else {
							wp_redirect( site_url() . '/login' );
						}
					?>
					<div id="personal-data" class="uk-modal">
						<div class="uk-modal-dialog">
							<h3>
								<?php echo __('Export Personal Data', 'phdhub-up'); ?>
								<a class="uk-modal-close uk-close"></a>
							</h3>
							<?php echo do_shortcode( '[gpdr-data-request]'); ?>
						</div>
					</div>
				</div>
			</div>
		</main><!-- #main -->
	</div><!-- #primary -->

<?php
get_footer();

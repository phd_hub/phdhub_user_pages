<?php
	defined('ABSPATH') or die;

	get_header();

	$firstname = (isset($_POST['firstname']) ? $_POST['firstname'] : null);
	$lastname = (isset($_POST['lastname']) ? $_POST['lastname'] : null);
	$email = (isset($_POST['email']) ? $_POST['email'] : null);
	$username = (isset($_POST['username']) ? $_POST['username'] : null);
	$password = (isset($_POST['password']) ? $_POST['password'] : null);
	$website = (isset($_POST['website']) ? $_POST['website'] : null);
	$facebook = (isset($_POST['fb_link']) ? $_POST['fb_link'] : null);
	$twitter = (isset($_POST['twitter_link']) ? $_POST['twitter_link'] : null);
	$linkedin = (isset($_POST['linkedin_link']) ? $_POST['linkedin_link'] : null);
	$gplus = (isset($_POST['gplus_link']) ? $_POST['gplus_link'] : null);
	$create_account = (isset($_POST['create_account']) ? $_POST['create_account'] : null);

	if ($create_account) {
		$userdata = array(
			'user_login'  =>  sanitize_user( $username, true ),
			'first_name'  =>  sanitize_text_field( $firstname ),
			'last_name'   =>  sanitize_text_field( $lastname ),
			'user_email'  =>  sanitize_email( $email ),
			'user_url'    =>  sanitize_text_field( $website ),
			'user_pass'   =>  $password
		);

		$user_id = wp_insert_user( $userdata ) ;
		add_user_meta( $user_id, 'fb_link', $facebook );
		add_user_meta( $user_id, 'twitter_link', $twitter );
		add_user_meta( $user_id, 'linkedin_link', $linkedin );
		add_user_meta( $user_id, 'gplus_link', $gplus );
	}
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<div id="registration-page">
			<header class="entry-header cpt-header">
				<div class="container">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div>
			</header><!-- .entry-header -->
			
			<div class="container">
				<div class="uk-grid">
					<div class="uk-width-7-10">
						<?php
							if (! is_user_logged_in()) {
						?>
						<form class="registration-form" method="POST">
							<p class="required-notice"><?php echo __('All fields with an asterisk are required!', 'phdhub-up'); ?></p>
							<div class="uk-grid">
								<div class="uk-width-1-1">
									<h3><?php echo __('Basic Info', 'phdhub-eu'); ?></h3>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('First Name', 'phdhub-up'); ?>*</label>
										<input type="text" name="firstname" required>
									</p>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('Last Name', 'phdhub-up'); ?>*</label>
										<input type="text" name="lastname" required>
									</p>
								</div>
								<div class="uk-width-1-1">
									<p>
										<label><?php echo __('Username', 'phdhub-up'); ?>*</label>
										<input type="text" name="username" required>
									</p>
								</div>
								<div class="uk-width-1-1">
									<p>
										<label><?php echo __('Email', 'phdhub-up'); ?>*</label>
										<input type="email" name="email" required>
									</p>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('Password', 'phdhub-up'); ?>*</label>
										<input type="password" id="password" name="password" required>
										<span id="password-strength"></span>
									</p>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('Repeat Password', 'phdhub-up'); ?>*</label>
										<input type="password" id="repeat-password" name="repeat_password" required>
									</p>
								</div>
								<div class="uk-width-1-1">
									<p>
										<label><?php echo __('Website', 'phdhub-up'); ?></label>
										<input type="url" name="website">
									</p>
								</div>

								<div class="uk-width-1-1">
									<h3><?php echo __('Social Networks', 'phdhub-up'); ?></h3>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('Facebook', 'phdhub-up'); ?></label>
										<input type="url" name="fb_link">
									</p>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('Twitter', 'phdhub-up'); ?></label>
										<input type="url" name="twitter_link">
									</p>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('LinkedIn', 'phdhub-up'); ?></label>
										<input type="url" name="linkedin_link">
									</p>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('Google+', 'phdhub-up'); ?></label>
										<input type="url" name="gplus_link">
									</p>
								</div>
								
								<div class="uk-width-1-1">
									<h3><?php echo __('Qualifications', 'phdhub-up'); ?></h3>
									<p>
										<textarea name="qualifications" rows="3"></textarea>
										<span class="field-info"><?php echo __('Separate qualifications with commas.', 'phdhub-up'); ?></span>
									</p>
								</div>
								
								<div class="uk-width-1-1">
									<h3><?php echo __('Fields of Science', 'phdhub-up'); ?></h3>
									<span class="field-info"><?php echo __('I am interested in the following Fields of Science', 'phdhub-up'); ?></span>
									<div class="uk-grid fields-of-science-list">
										<div class="uk-width-1-2">
											<ul data-uk-switcher="{connect:'#fos-list'}">
												<?php 
													$hiterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => 0));
													foreach($hiterms as $key => $hiterm) {
												?>
												<li>
													<a href="">
														<?php echo $hiterm->name; ?>
													</a>
												</li>
												<?php
													}
												?>
											</ul>
										</div>
										<div class="uk-width-1-2">
											<ul id="fos-list" class="uk-switcher">
												<?php 
													foreach($hiterms as $key => $hiterm) {
												?>
												<li>
													<span class="checkbox-row">
														<input type="checkbox" name="field_of_science" value="<?php echo $hiterm->term_id; ?>"><?php echo $hiterm->name; ?>
													</span>
												<?php
														$loterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => $hiterm->term_id));
														foreach($loterms as $key => $loterm) {
												?>
													<span class="checkbox-row">
														<input type="checkbox" class="child" name="field_of_science" value="<?php echo $loterm->term_id; ?>"><?php echo $loterm->name; ?>
													</span>
												<?php 
														}
												?>
												</li>
												<?php
													}
												?>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<input type="submit" disabled="disabled" name="create_account" value="<?php echo __('Create Account', 'phdhub-up'); ?>" />
						</form>
						<?php
							} else {
								$current_user = wp_get_current_user();
						?>
						<div class="is-logged-in">
							<h3><?php echo __('Welcome', 'phdhub-up') . ' ' . $current_user->user_firstname; ?></h3>
							<p>
								<?php echo __('It seems that you already have an account!', 'phdhub-up'); ?>
							</p>
							<p>
								<?php echo __('If this is not your account, please logout now and then login with your account.', 'phdhub-up'); ?>
							</p>
							<p class="logout">
								<a href="<?php echo wp_logout_url( home_url() ); ?>"><?php echo __('Logout', 'phdhub-up'); ?></a>
							</p>
						</div>
						<?php
							}
						?>
					</div>
					
					<div class="uk-width-3-10">
						<?php
							if (! is_user_logged_in() ) {
						?>
						<div data-uk-sticky="{boundary: true}">
							<div class="login-box-link">
								<p>
									<span><?php echo __('If you already have an account, please login using the following link', 'phdhub-up'); ?>:</span>
									<a href="<?php echo site_url() . '/login'; ?>"><?php echo __('Login Now', 'phdhub-up'); ?></a>
								</p>
							</div>
						</div>
						<?php
							} else {
						?>
						<div class="is-logged-in">
							<h3><?php echo __('My Account', 'phdhub-up'); ?></h3>
							<ul class="account-links">
								<li><i class="fa fa-user-circle-o"></i> <a href="<?php echo site_url() . '/profile'; ?>"><?php echo __('Profile', 'phdhub-up'); ?></a></li>
								<li><i class="fa fa-cogs"></i> <a href="<?php echo site_url() . '/account-settings'; ?>"><?php echo __('Settings', 'phdhub-up'); ?></a></li>
							</ul>
						</div>
						<?php
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</main>
</div>
<?php
	get_footer();
?>
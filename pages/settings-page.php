<?php
	defined('ABSPATH') or die;

	get_header();

	$firstname = (isset($_POST['firstname']) ? $_POST['firstname'] : null);
	$lastname = (isset($_POST['lastname']) ? $_POST['lastname'] : null);
	$email = (isset($_POST['email']) ? $_POST['email'] : null);
	$password = (isset($_POST['password']) ? $_POST['password'] : null);
	$website = (isset($_POST['website']) ? $_POST['website'] : null);
	$facebook = (isset($_POST['fb_link']) ? $_POST['fb_link'] : null);
	$twitter = (isset($_POST['twitter_link']) ? $_POST['twitter_link'] : null);
	$linkedin = (isset($_POST['linkedin_link']) ? $_POST['linkedin_link'] : null);
	$gplus = (isset($_POST['gplus_link']) ? $_POST['gplus_link'] : null);
	$fields_of_science_list = (isset($_POST['fields_of_science']) ? $_POST['fields_of_science'] : null);
	$research_areas_list = (isset($_POST['research_areas']) ? $_POST['research_areas'] : null);
	$qualifications_list = (isset($_POST['skills']) ? $_POST['skills'] : null);
	$update_account = (isset($_POST['update_account']) ? $_POST['update_account'] : null);

	$current_user = wp_get_current_user();
	$user_id = $current_user->ID;
	if ($update_account) {
		if (!empty ($password)) {
			$userdata = array(
				'ID' => $user_id,
				'first_name'  =>  sanitize_text_field( $firstname ),
				'last_name'   =>  sanitize_text_field( $lastname ),
				'user_email'  =>  sanitize_email( $email ),
				'user_url'    =>  sanitize_text_field( $website ),
				'user_pass'   =>  $password
			);
		} else {
			$userdata = array(
				'ID' => $user_id,
				'first_name'  =>  sanitize_text_field( $firstname ),
				'last_name'   =>  sanitize_text_field( $lastname ),
				'user_email'  =>  sanitize_email( $email ),
				'user_url'    =>  sanitize_text_field( $website ),
			);
		}

		wp_update_user( $userdata ) ;
		update_user_meta( $user_id, 'fb_link', $facebook );
		update_user_meta( $user_id, 'twitter_link', $twitter );
		update_user_meta( $user_id, 'linkedin_link', $linkedin );
		update_user_meta( $user_id, 'gplus_link', $gplus );
		update_user_meta( $user_id, 'fields_of_science', $fields_of_science_list );
		update_user_meta( $user_id, 'research_areas', $research_areas_list );
		update_user_meta( $user_id, 'skills', $qualifications_list );
	}
	$user_info = get_userdata($user_id);
	$fb_link = get_user_meta( $user_id, 'fb_link', true );
	$twitter_link = get_user_meta( $user_id, 'twitter_link', true );
	$linkedin_link = get_user_meta( $user_id, 'linkedin_link', true );
	$gplus_link = get_user_meta( $user_id, 'gplus_link', true );
	$fields_of_science_list = get_the_author_meta( 'fields_of_science', $user_id );
	$research_areas_list = get_the_author_meta( 'research_areas', $user_id );
	$qualifications_list = get_the_author_meta( 'skills', $user_id );
?>
<div id="primary" class="content-area">
	<main id="main" class="site-main">
		<div id="registration-page">
			<header class="entry-header cpt-header">
				<div class="container">
					<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
				</div>
			</header><!-- .entry-header -->
			
			<div class="container">
				<div class="uk-grid">
					<div class="uk-width-7-10">
						<?php
							if ( is_user_logged_in()) {
						?>
						<form class="registration-form" method="POST">
							<p class="required-notice"><?php echo __('All fields with an asterisk are required!', 'phdhub-up'); ?></p>
							<p><?php echo get_avatar( $user_id ); ?></p>
							<a class="profile-img-link" href="#profile-img" data-uk-modal><?php echo __('Update Profile Image', 'phdhub-up'); ?></a>
							<div class="uk-grid">
								<div class="uk-width-1-1">
									<h3><?php echo __('Basic Info', 'phdhub-eu'); ?></h3>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('First Name', 'phdhub-up'); ?>*</label>
										<input type="text" name="firstname" value="<?php echo $user_info->first_name; ?>" required>
									</p>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('Last Name', 'phdhub-up'); ?>*</label>
										<input type="text" name="lastname" value="<?php echo $user_info->last_name; ?>" required>
									</p>
								</div>
								<div class="uk-width-1-1">
									<p>
										<label><?php echo __('Username', 'phdhub-up'); ?>*</label>
										<input type="text" name="username" value="<?php echo $user_info->user_login; ?>" disabled>
									</p>
								</div>
								<div class="uk-width-1-1">
									<p>
										<label><?php echo __('Email', 'phdhub-up'); ?>*</label>
										<input type="email" name="email" value="<?php echo $user_info->user_email; ?>" required>
									</p>
								</div>
								<div class="uk-width-1-1">
									<p>
										<label><?php echo __('Website', 'phdhub-up'); ?></label>
										<input type="url" name="website" value="<?php echo $user_info->user_url; ?>">
									</p>
								</div>

								<div class="uk-width-1-1">
									<h3><?php echo __('Social Networks', 'phdhub-up'); ?></h3>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('Facebook', 'phdhub-up'); ?></label>
										<input type="url" name="fb_link" value="<?php echo $fb_link; ?>">
									</p>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('Twitter', 'phdhub-up'); ?></label>
										<input type="url" name="twitter_link" value="<?php echo $twitter_link; ?>">
									</p>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('LinkedIn', 'phdhub-up'); ?></label>
										<input type="url" name="linkedin_link" value="<?php echo $linkedin_link; ?>">
									</p>
								</div>
								<div class="uk-width-1-2">
									<p>
										<label><?php echo __('Google+', 'phdhub-up'); ?></label>
										<input type="url" name="gplus_link" value="<?php echo $gplus_link; ?>">
									</p>
								</div>
								
								<?php
									if ( in_array( 'subscriber', (array) $current_user->roles ) ||  in_array( 'lead-researcher', (array) $current_user->roles ) || in_array( 'assist-researcher', (array) $current_user->roles ) ) {
								?>
								<div class="uk-width-1-1">
									<h3><?php echo __('Fields of Science', 'phdhub-up'); ?></h3>
									<span class="field-info"><?php echo __('I am interested in the following Fields of Science', 'phdhub-up'); ?></span>
									<div class="uk-grid fields-of-science-list">
										<div class="uk-width-1-2">
											<ul data-uk-switcher="{connect:'#fos-list'}">
												<?php 
												if ($fields_of_science_list == NULL) {
													$fields_of_science_list = array();
												}
													$hiterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => 0));
													foreach($hiterms as $key => $hiterm) {
												?>
												<li>
													<a href=""><?php echo $hiterm->name; ?></a>
												</li>
												<?php
													}
												?>
											</ul>
										</div>
										<div class="uk-width-1-2">
											<ul id="fos-list" class="uk-switcher">
												<?php 
													foreach($hiterms as $key => $hiterm) {
												?>
												<li>
													<span class="checkbox-row">
														<input type="checkbox" name="fields_of_science[]" value="<?php echo $hiterm->term_id; ?>" <?php checked(in_array($hiterm->term_id, $fields_of_science_list)); ?>><?php echo $hiterm->name; ?>
													</span>
													<?php
														$loterms = get_terms("fields-of-science", array("hide_empty" => false, "parent" => $hiterm->term_id));
														foreach($loterms as $key => $loterm) {
															$loterms2 = get_terms("fields-of-science", array("hide_empty" => false, "parent" => $loterm->term_id));
													?>
													<span class="checkbox-row">
														<input type="checkbox" class="child" name="fields_of_science[]" value="<?php echo $loterm->term_id; ?>" <?php checked(in_array($loterm->term_id, $fields_of_science_list)); ?>><?php echo $loterm->name; ?>
													</span>
														<?php
															if ($loterms2) {
														?>
														<ul class="children-fos">
															<?php
																foreach ($loterms2 as $key => $loterm2) {
															?>
															<li>
																<span class="checkbox-row">
																	<input type="checkbox" class="child" name="fields_of_science[]" value="<?php echo $loterm2->term_id; ?>" <?php checked(in_array($loterm2->term_id, $fields_of_science_list)); ?>><?php echo $loterm2->name; ?>
																</span>
															</li>
															<?php
																}
															?>
														</ul>
													<?php 
															} 
														}
													?>
												</li>
												<?php
													}
												?>
											</ul>
										</div>
									</div>
								</div>
								<div class="uk-width-1-1">
									<h3><?php echo __('Research Areas', 'phdhub-up'); ?></h3>
									<span class="field-info"><?php echo __('I am interested in the following Research Areas', 'phdhub-up'); ?></span>
									<div class="research-areas-list">
										<ul class="ra-list">
											<?php 
												if ($research_areas_list == NULL) {
													$research_areas_list = array();
												}
												$hiterms = get_terms("research-areas", array("hide_empty" => false));
												foreach($hiterms as $key => $hiterm) {
											?>
											<li>
												<span class="checkbox-row">
													<input type="checkbox" name="research_areas[]" value="<?php echo $hiterm->term_id; ?>" <?php checked(in_array($hiterm->term_id, $research_areas_list)); ?>><?php echo $hiterm->name; ?>
												</span>
											</li>
											<?php
												}
											?>
										</ul>
									</div>
								</div>
								<div class="uk-width-1-1">
									<h3><?php echo __('Qualifications', 'phdhub-up'); ?></h3>
									<span class="field-info"><?php echo __('Select your skills from the following list', 'phdhub-up'); ?></span>
									<div class="research-areas-list">
										<ul class="ra-list">
											<?php 
												if ($qualifications_list == NULL) {
													$qualifications_list = array();
												}
												$hiterms = get_terms("skills", array("hide_empty" => false));
												foreach($hiterms as $key => $hiterm) {
											?>
											<li>
												<span class="checkbox-row">
													<input type="checkbox" name="skills[]" value="<?php echo $hiterm->term_id; ?>" <?php checked(in_array($hiterm->term_id, $qualifications_list)); ?>><?php echo $hiterm->name; ?>
												</span>
											</li>
											<?php
												}
											?>
										</ul>
									</div>
								</div>
								<?php
									}
								?>
								
							</div>
							<input type="submit" name="update_account" value="<?php echo __('Update Account', 'phdhub-up'); ?>" />
						</form>
						<div id="profile-img" class="uk-modal">
							<div class="uk-modal-dialog">
								<h3>
									<?php echo __('Update Profile Image', 'phdhub-up'); ?>
									<a class="uk-modal-close uk-close"></a>
								</h3>
								<?php echo do_shortcode( '[avatar_upload]'); ?>
							</div>
						</div>
						<?php
							} else {
								wp_redirect( site_url() . '/login' );
							}
						?>
					</div>
					
					<div class="uk-width-3-10">
						<?php
							if (! is_user_logged_in() ) {
						?>
						<div data-uk-sticky="{boundary: true}">
							<div class="login-box-link">
								<p>
									<span><?php echo __('If you already have an account, please login using the following link', 'phdhub-up'); ?>:</span>
									<a href="<?php echo site_url() . '/login'; ?>"><?php echo __('Login Now', 'phdhub-up'); ?></a>
								</p>
							</div>
						</div>
						<?php
							} else {
						?>
						<div class="is-logged-in">
							<h3><?php echo __('My Account', 'phdhub-up'); ?></h3>
							<ul class="account-links">
								<li><i class="fa fa-user-circle-o"></i> <a href="<?php echo site_url() . '/profile'; ?>"><?php echo __('Profile', 'phdhub-up'); ?></a></li>
								<li><i class="fa fa-cogs"></i> <a href="<?php echo site_url() . '/account-settings'; ?>"><?php echo __('Account Settings', 'phdhub-up'); ?></a></li>
								<li><i class="fa fa-lock"></i> <a href="<?php echo site_url() . '/password-settings'; ?>"><?php echo __('Password Settings', 'phdhub-up'); ?></a></li>
							</ul>
						</div>
						<?php
							}
						?>
					</div>
				</div>
			</div>
		</div>
	</main>
</div>
<?php
	get_footer();
?>
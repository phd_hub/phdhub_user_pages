<?php
/*
Plugin Name: PhD Hub User Pages
Plugin URI: http://it.auth.gr
Description: Custom pages for PhD Hub's users
Text Domain: phdhub-up
Domain Path: /lang
Version: 2.0.0
Author: Ioannis Zachros
Author URI: https:/it.auth.gr
License: GPLv2 or later
*/

defined('ABSPATH') or die;


add_action( 'plugins_loaded', 'phdhub_up_load_textdomain' );
function phdhub_up_load_textdomain() {
	load_plugin_textdomain( 'phdhub-up', false, dirname( plugin_basename(__FILE__) ) . '/lang/' ); 
}

define("PHDHUB_USER_PAGES_PLUGIN_DIR", dirname(plugin_basename(__FILE__)));

/*
 * Include the main file
 */
include_once dirname( __FILE__ ) . '/inc/config.php';

?>
